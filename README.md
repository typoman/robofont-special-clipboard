# RoboFont Special Clipboard
![](PasteSpecial.roboFontExt/html/robofont-paste-special.png)

Use `⌘ + ⇧ + V` to paste with more options in RoboFont.

## Features
- Replaces only the selected data (e.g contours, anchors, components) on paste.
- Only pastes on selected glyphs. If no glyph is selected it tries to find the target glyph using unicode or glyph name.
- On paste, it pastes on glyph with same unicode value. If a glyph doesn't have unicode, it uses glyph name.
- Doesn't add a new glyph, only changes existing glyphs.
- Pastes to the selected layer.
- Copy data from one glyph and paste to multiple glyphs.
- It adds a variable to all the open fonts named `clipboardGlyphs` which is a list of glyphs in the clipboard.